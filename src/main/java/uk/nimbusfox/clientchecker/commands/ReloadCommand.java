package uk.nimbusfox.clientchecker.commands;

import com.velocitypowered.api.command.SimpleCommand;
import uk.nimbusfox.clientchecker.Config;

public final class ReloadCommand implements SimpleCommand {
    @Override
    public void execute(final Invocation invocation) {
        Config.Reload();
    }

    @Override
    public boolean hasPermission(final Invocation invocation) {
        return invocation.source().hasPermission("clientchecker.reload");
    }
}
