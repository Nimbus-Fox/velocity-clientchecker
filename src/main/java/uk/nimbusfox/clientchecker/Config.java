package uk.nimbusfox.clientchecker;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@ApiStatus.Internal
public class Config {
    private static HashMap<String, ArrayList<String>> clients = new HashMap<>();
    public static Path dataPath;
    public static Logger logger;
    private static Path configPath;
    private final static Gson gson = new Gson();

    public static void Reload() {
        if (configPath == null) {
            configPath = Paths.get(dataPath.toString(), "config.json");
        }

        if (!Files.exists(dataPath)) {
            try {
                Files.createDirectory(dataPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!Files.exists(configPath)) {
            try {
                Files.writeString(configPath, "{\n    \"_default_\": [\n        \"lpv user {player} permission set meta.client.{client}\"\n    ]\n}");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        clients.clear();

        try {
            clients = gson.fromJson(Files.readString(configPath), java.util.HashMap.class);
        } catch (IOException | JsonSyntaxException e) {
            e.printStackTrace();
        }

        logger.info(clients.size() + " clients loaded from config");
    }

    public static @NotNull String[] GetClientCommands(String brand) {
        ArrayList<String> commands = new ArrayList<>();

        if (clients.containsKey("_default_")) {
            var comms = clients.get("_default_");
            for (var i = 0; i < comms.size(); i++) {
                commands.add(comms.get(i));
            }
        }

        var keys = clients.keySet().toArray(new String[0]);
        for (var i = 0; i < clients.size(); i++) {
            if (brand.toLowerCase(Locale.ROOT).equals(keys[i].toLowerCase(Locale.ROOT))) {
                commands.addAll(clients.get(keys[i]));
                break;
            }
        }

        return commands.toArray(new String[0]);
    }
}
