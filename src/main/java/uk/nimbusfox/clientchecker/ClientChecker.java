package uk.nimbusfox.clientchecker;

import com.google.inject.Inject;
import com.velocitypowered.api.command.CommandManager;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.event.player.PlayerClientBrandEvent;
import com.velocitypowered.api.event.connection.DisconnectEvent;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.plugin.annotation.DataDirectory;
import com.velocitypowered.api.proxy.ProxyServer;
import org.slf4j.Logger;
import uk.nimbusfox.clientchecker.commands.ReloadCommand;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

@Plugin(
        id = "clientchecker",
        name = "ClientChecker",
        version = "1.0-SNAPSHOT",
        description = "Plugin to check what client the player is connecting with and run commands based on the client",
        authors = {"NimbusFox"}
)
public class ClientChecker {

    private final Logger logger;
    private final ProxyServer server;
    @Inject
    private CommandManager manager;

    private final HashSet<UUID> ignore = new HashSet<>();

    @Inject
    public ClientChecker(ProxyServer server, Logger logger, @DataDirectory Path dataDirectory) {
        this.logger = logger;
        this.server = server;
        Config.logger = logger;
        Config.dataPath = dataDirectory;

        Config.Reload();
    }

    @Subscribe
    public void onProxyInitialization(ProxyInitializeEvent event) {
        var meta = manager.metaBuilder("ccreload").build();

        manager.register(meta, new ReloadCommand());
    }

    @Subscribe
    public void onPlayerBrand(PlayerClientBrandEvent event) {
        var player = event.getPlayer();
        if (ignore.contains(player.getUniqueId())) {
            return;
        }
        var brand = event.getBrand();
        var commands = Config.GetClientCommands(brand);
        for (var i = 0; i < commands.length; i++) {
            var parse = commands[i];
            if (parse.contains("{player}")) {
                parse = parse.replace("{player}", player.getUsername());
            }

            if (parse.contains("{client}")) {
                parse = parse.replace("{client}", brand);
            }

            manager.executeAsync(server.getConsoleCommandSource(), parse);
        }

        logger.info(event.getPlayer().getUsername() + " connected with " + brand + " client");
    }

    @Subscribe
    public void onDisconnect(DisconnectEvent event) {
        ignore.remove(event.getPlayer().getUniqueId());
    }
}
